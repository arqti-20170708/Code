package com.example.rest.bean;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;

public class Suma extends ResourceSupport implements Serializable {

	private static final long serialVersionUID = -6859399399798770474L;

	private int sumando1;
	private int sumando2;
	private int total;

	public Suma() {
		super();
		this.sumando1 = 0;
		this.sumando2 = 0;
		this.total = 0;
	}

	public Suma(int sumando1, int sumando2) {
		super();
		this.sumando1 = sumando1;
		this.sumando2 = sumando2;
		this.total = 0;
	}

	public Suma(int sumando1, int sumando2, int total) {
		super();
		this.sumando1 = sumando1;
		this.sumando2 = sumando2;
		this.total = total;
	}

	public int getSumando1() {
		return sumando1;
	}

	public void setSumando1(int sumando1) {
		this.sumando1 = sumando1;
	}

	public int getSumando2() {
		return sumando2;
	}

	public void setSumando2(int sumando2) {
		this.sumando2 = sumando2;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Suma [sumando1=" + sumando1 + ", sumando2=" + sumando2 + ", total=" + total + "]";
	}
}