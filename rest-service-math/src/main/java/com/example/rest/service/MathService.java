package com.example.rest.service;

import com.example.rest.bean.Suma;

public interface MathService {

	Suma doSuma(int sumando1, int sumando2);
}
