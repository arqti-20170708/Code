package com.example.rest.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.rest.bean.Suma;
import com.example.rest.service.MathService;

@Service
public class MathServiceImpl implements MathService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MathServiceImpl.class);

	@Override
	public Suma doSuma(int sumando1, int sumando2) {
		LOGGER.debug("Ejecutando la operación de suma");
		int total = sumando1 + sumando2;
		Suma resultado = new Suma(sumando1, sumando2, total);
		LOGGER.debug("Resultado de la suma: " + resultado.toString());
		return resultado;
	}
}
