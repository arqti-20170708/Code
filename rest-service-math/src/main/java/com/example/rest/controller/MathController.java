package com.example.rest.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.rest.bean.Suma;
import com.example.rest.exception.ApiGeneralException;
import com.example.rest.service.MathService;

@RestController
@RequestMapping("/suma")
public class MathController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MathController.class);
	private static final String API_VERSION = "1.0";

	@Autowired
	private MathService mathService;

	@RequestMapping(value = "/v"
			+ API_VERSION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Suma doSumaRequestParam(@RequestParam(value = "sumando1", required = true) int sumando1,
			@RequestParam(value = "sumando2", required = true) int sumando2) throws ApiGeneralException {
		LOGGER.debug("Se ha solicitado ejecutar la operación de suma de dos valores");

		Suma suma = mathService.doSuma(sumando1, sumando2);

		List<Link> links = new ArrayList<Link>();
		links.add(linkTo(methodOn(MathController.class).doSumaRequestParam(sumando1, sumando2)).withSelfRel());
		suma.add(links);

		LOGGER.debug("Se ha salido del método 'readUser' del 'userService'");
		return suma;
	}
}
