# Code



## rest-service-jersey2 : Proyecto REST de prueba usando únicamente Jersey 2

### Comando para compilar y empaquetar
En el directorio de `rest-service-jersey2`
```
$> mvn clean compile -DskipTests=true ; mvn package
```



## example-rs : POM Parent de ejemplo para servicios RESTful

### Comando para compilar, empaquetar e instalar
En el directorio de `example-rs`
```
$> mvn clean compile ; mvn package ; mvn install
```



## rest-service-exceptions : Librer&iacute;a con las clases de errores para servicios REST

### Comando para compilar, empaquetar e instalar
En el directorio de `rest-service-exceptions`
```
$> mvn clean compile -DskipTest=true ; mvn package -DskipTest=true ; mvn install -DskipTest=true
```



## rest-service-user : Proyecto para API RESTful para generar menús multiempresa

### Comando para compilar y empaquetar
En el directorio de `rest-service-user`
```
$> mvn clean compile -P{jboss|weblogic|wso2as} -Denvironment={DEV|QA|PROD} -DskipTests=true ; mvn package -P{jboss|weblogic|wso2as} -Denvironment={DEV|QA|PROD}
```
por ejemplo, para Apache Tomcat:
```
$> mvn clean compile -Denvironment=DEV -DskipTests=true ; mvn package -Denvironment=DEV
```

### Ubicación de Swagger UI
{scheme}://{hostname}:{port}/menu-rest/swagger-ui.html

### Ubicación de documentación de API generada por Swagger en formato JSON
{scheme}://{hostname}:{port}/menu-rest/v2/api-docs[.json]

### Referencias
#### HTTP Message Converters:
* [Understanding HTTP Message Converters in Spring Framework - jCombat](http://www.jcombat.com/spring/understanding-http-message-converters-in-spring-framework)
* [Http Message Converters with the Spring Framework | Baeldung](http://www.baeldung.com/spring-httpmessageconverter-rest)

#### HATEOAS:
* [An Intro to Spring HATEOAS | Baeldung](http://www.baeldung.com/spring-hateoas-tutorial)

#### Swagger:
* [Springfox Reference Documentation](https://springfox.github.io/springfox/docs/current/)
* [Setting Up Swagger 2 with a Spring REST API | Baeldung](http://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)
