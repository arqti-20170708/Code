package com.example.rest.bean;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada como bean para la empresa con colecci&oacute;n de nodos (el
 * men&uacute;)
 * 
 */
public class EmpresaMenu extends EmpresaBase {

    private static final long serialVersionUID = -4418898497115465497L;

    private Menu menu;

    /**
     * Constructor por defecto
     */
    public EmpresaMenu() {
        super();
        this.menu = new Menu();
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param cveEmpresa
     *            Clave de la empresa
     * @param descripcionEmpresa
     *            Descripci&oacute;n de la empresa
     */
    public EmpresaMenu(String cveEmpresa, String descripcionEmpresa) {
        super(cveEmpresa, descripcionEmpresa);
        this.menu = new Menu();
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param cveEmpresa
     *            Clave de la empresa
     * @param descripcionEmpresa
     *            Descripci&oacute;n de la empresa
     * @param menu
     *            Colecci&oacute;n de elementos del modelo de MenuApp
     */
    public EmpresaMenu(String cveEmpresa, String descripcionEmpresa, Menu menu) {
        super(cveEmpresa, descripcionEmpresa);
        this.menu = menu;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param empresaMenuOriginal
     *            Empresa con men&uacute; original del que se copiar&aacute;n
     *            los atributos
     */
    public EmpresaMenu(EmpresaMenu empresaMenuOriginal) {
        super(empresaMenuOriginal.getCveEmpresa(), empresaMenuOriginal.getDescripcionEmpresa());
        this.menu = empresaMenuOriginal.getMenu();
    }

    /**
     * @return Colecci&oacute;n de elementos del modelo de MenuApp
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * @param menu
     *            Colecci&oacute;n de elementos del modelo de MenuApp
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "EmpresaMenu [" + super.toString() + ", menu=" + menu + "]";
    }
}
