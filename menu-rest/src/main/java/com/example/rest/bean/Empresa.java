package com.example.rest.bean;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada como bean para la empresa
 * 
 */
public class Empresa extends EmpresaBase {

    private static final long serialVersionUID = -6486316005654125717L;

    /**
     * Constructor por defecto
     */
    public Empresa() {
        super();
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param cveEmpresa
     *            Clave de la empresa
     * @param descripcionEmpresa
     *            Descripci&oacute;n de la empresa
     */
    public Empresa(String cveEmpresa, String descripcionEmpresa) {
        super(cveEmpresa, descripcionEmpresa);
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param empresaOriginal
     *            Empresa original del que se copiar&aacute;n
     *            los atributos
     */
    public Empresa(Empresa empresaOriginal) {
        super(empresaOriginal.getCveEmpresa(), empresaOriginal.getDescripcionEmpresa());
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Empresa [" + super.toString() + "]";
    }
}
