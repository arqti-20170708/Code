package com.example.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada para englobar una colecci&oacute;n de empresas con
 * colecci&oacute;n de nodos (el men&uacute;)
 * 
 */
public class EmpresaMenuCollection extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = -9105244461274342851L;

    private List<EmpresaMenu> empresas;

    /**
     * Constructor por defecto
     */
    public EmpresaMenuCollection() {
        super();
        this.empresas = new ArrayList<EmpresaMenu>();
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param empresas
     *            Colecci&oacute;n de empresas con colecci&oacute;n de nodos (el
     *            men&uacute;)
     */
    public EmpresaMenuCollection(List<EmpresaMenu> empresas) {
        super();
        this.empresas = empresas;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param empresaMenuCollectionOriginal
     *            EmpresaMenuCollection original del que se copiar&aacute;n los
     *            atributos
     */
    public EmpresaMenuCollection(EmpresaMenuCollection empresaMenuCollectionOriginal) {
        super();
        this.empresas = empresaMenuCollectionOriginal.getEmpresas();
    }

    /**
     * @return Colecci&oacute;n de empresas con colecci&oacute;n de nodos (el
     *         men&uacute;)
     */
    public List<EmpresaMenu> getEmpresas() {
        return empresas;
    }

    /**
     * @param empresas
     *            Colecci&oacute;n de empresas con colecci&oacute;n de nodos (el
     *            men&uacute;)
     */
    public void setEmpresas(List<EmpresaMenu> empresas) {
        this.empresas = empresas;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "EmpresaMenuCollection [empresas=" + empresas + "]";
    }
}
