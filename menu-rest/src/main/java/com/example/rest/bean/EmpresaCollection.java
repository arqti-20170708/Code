package com.example.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada para englobar una colecci&oacute;n de empresas
 * 
 */
public class EmpresaCollection extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = -9105244461274342851L;

    private List<Empresa> empresas;

    /**
     * Constructor por defecto
     */
    public EmpresaCollection() {
        super();
        this.empresas = new ArrayList<Empresa>();
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param empresas
     *            Colecci&oacute;n de empresas
     */
    public EmpresaCollection(List<Empresa> empresas) {
        super();
        this.empresas = empresas;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param empresaCollectionOriginal
     *            EmpresaCollection original del que se copiar&aacute;n los
     *            atributos
     */
    public EmpresaCollection(EmpresaCollection empresaCollectionOriginal) {
        super();
        this.empresas = empresaCollectionOriginal.getEmpresas();
    }

    /**
     * @return Colecci&oacute;n de empresas
     */
    public List<Empresa> getEmpresas() {
        return empresas;
    }

    /**
     * @param empresas
     *            Colecci&oacute;n de empresas
     */
    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "EmpresaCollection [empresas=" + empresas + "]";
    }
}
