package com.example.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada para englobar una colecci&oacute;n de nodos (el men&uacute;)
 * 
 */
public class Menu extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = 2447749953261555844L;

    private List<NodoMenu> nodos;

    /**
     * Constructor por defecto
     */
    public Menu() {
        super();
        this.nodos = new ArrayList<NodoMenu>();
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param nodos
     *            Colecci&oacute;n de nodos de men&uacute;
     */
    public Menu(List<NodoMenu> nodos) {
        super();
        this.nodos = nodos;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param menuOriginal
     *            Men original del que se copiar&aacute;n los atributos
     */
    public Menu(Menu menuOriginal) {
        super();
        this.nodos = menuOriginal.getNodos();
    }

    /**
     * @return Colecci&oacute;n de nodos de men&uacute;
     */
    public List<NodoMenu> getNodos() {
        return nodos;
    }

    /**
     * @param nodos
     *            Colecci&oacute;n de nodos de men&uacute;
     */
    public void setNodos(List<NodoMenu> nodos) {
        this.nodos = nodos;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Menu [nodos=" + nodos + "]";
    }
}
