package com.example.rest.bean;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;

/**
 * {@inheritDoc}
 * 
 * Clase de la que heredar&aacute;n {@link com.example.rest.bean.Empresa
 * Empresa} y {@link com.example.rest.bean.EmpresaMenu EmpresaMenu} dependiendo
 * de la cantidad de informaci&oacute;n que se quiera presentar en los recursos
 * del servicio RESTful
 * 
 */
public class EmpresaBase extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = 8385333486846695262L;

    private String cveEmpresa;
    private String descripcionEmpresa;

    /**
     * Constructor por defecto
     */
    public EmpresaBase() {
        super();
        this.cveEmpresa = "";
        this.descripcionEmpresa = "";
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param cveEmpresa
     *            Clave de la empresa
     * @param descripcionEmpresa
     *            Descripci&oacute;n de la empresa
     */
    public EmpresaBase(String cveEmpresa, String descripcionEmpresa) {
        super();
        this.cveEmpresa = cveEmpresa;
        this.descripcionEmpresa = descripcionEmpresa;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param empresaBaseOriginal
     *            EmpresaBase original del que se copiar&aacute;n los atributos
     */
    public EmpresaBase(EmpresaBase empresaBaseOriginal) {
        super();
        this.cveEmpresa = empresaBaseOriginal.getCveEmpresa();
        this.descripcionEmpresa = empresaBaseOriginal.getDescripcionEmpresa();
    }

    /**
     * @return Clave de la empresa
     */
    public String getCveEmpresa() {
        return cveEmpresa;
    }

    /**
     * @param cveEmpresa
     *            Clave de la empresa
     */
    public void setCveEmpresa(String cveEmpresa) {
        this.cveEmpresa = cveEmpresa;
    }

    /**
     * @return Descripci&oacute;n de la empresa
     */
    public String getDescripcionEmpresa() {
        return descripcionEmpresa;
    }

    /**
     * @param descripcionEmpresa
     *            Descripci&oacute;n de la empresa
     */
    public void setDescripcionEmpresa(String descripcionEmpresa) {
        this.descripcionEmpresa = descripcionEmpresa;
    }

    @Override
    public String toString() {
        return "EmpresaBase [cveEmpresa=" + cveEmpresa + ", descripcionEmpresa=" + descripcionEmpresa + "]";
    }
}
