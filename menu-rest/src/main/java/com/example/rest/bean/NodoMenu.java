package com.example.rest.bean;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada como bean para el nodo del men&uacute;
 * 
 */
public class NodoMenu extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = 5525229028111872644L;

    private int idAutoridad;
    private String detalleAutoridad;
    private String nivelPopMenu;
    private int idOpcMenu;
    private String urlPopMenu;
    private int grupoRefPopMenu;
    private String txPopMenu;
    private int posicionPopMenu;
    private String tipoNodo;
    private String icono;

    /**
     * Constructor por defecto
     */
    public NodoMenu() {
        super();
        this.idAutoridad = 0;
        this.detalleAutoridad = "";
        this.nivelPopMenu = "";
        this.idOpcMenu = 0;
        this.urlPopMenu = "";
        this.grupoRefPopMenu = 0;
        this.txPopMenu = "";
        this.posicionPopMenu = 0;
        this.tipoNodo = "";
        this.icono = "";
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param idAutoridad
     *            Identificador de la autoridad
     * @param detalleAutoridad
     *            El recurso a proteger (URL, clase y/o m&eacute;todo
     *            JavaScript, clase y/o m&eacute;todo Java)
     * @param nivelPopMenu
     *            Posici&oacute;n en el &aacute;rbol del men&uacute; cuando no
     *            se construye recursivamente
     * @param idOpcMenu
     *            Identificador de la opci&oacute;n de men&uacute;. Cuando se
     *            construye recursivamente, es el identificador del nodo en el
     *            men&uacute;
     * @param urlPopMenu
     *            Valor de HREF
     * @param grupoRefPopMenu
     *            El nodo padre de ese elemento de men&uacute; para
     *            construcci&oacute;n recursiva
     * @param txPopMenu
     *            Etiqueta para la opci&oacute;n de men&uacute;
     * @param posicionPopMenu
     *            Posici&oacute;n a partir del nodo padre para
     *            construcci&oacute;n recursiva
     * @param tipoNodo
     *            Estilo del nodo (S&oacute;lo puede ser 'R':ra&iacute;z o
     *            'O':opci&oacute;n)
     * @param icono
     *            V&iacute;nculo a icono que se mostrar&aacute; en el nodo
     */
    public NodoMenu(int idAutoridad, String detalleAutoridad, String nivelPopMenu, int idOpcMenu, String urlPopMenu,
            int grupoRefPopMenu, String txPopMenu, int posicionPopMenu, String tipoNodo, String icono) {
        super();
        this.idAutoridad = idAutoridad;
        this.detalleAutoridad = detalleAutoridad;
        this.nivelPopMenu = nivelPopMenu;
        this.idOpcMenu = idOpcMenu;
        this.urlPopMenu = urlPopMenu;
        this.grupoRefPopMenu = grupoRefPopMenu;
        this.txPopMenu = txPopMenu;
        this.posicionPopMenu = posicionPopMenu;
        this.tipoNodo = tipoNodo;
        this.icono = icono;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param nodoMenuOriginal
     *            Nodo original del menú del que se copiar&aacute;n los
     *            atributos
     */
    public NodoMenu(NodoMenu nodoMenuOriginal) {
        super();
        this.idAutoridad = nodoMenuOriginal.getIdAutoridad();
        this.detalleAutoridad = nodoMenuOriginal.getDetalleAutoridad();
        this.nivelPopMenu = nodoMenuOriginal.getNivelPopMenu();
        this.idOpcMenu = nodoMenuOriginal.getIdOpcMenu();
        this.urlPopMenu = nodoMenuOriginal.getUrlPopMenu();
        this.grupoRefPopMenu = nodoMenuOriginal.getGrupoRefPopMenu();
        this.txPopMenu = nodoMenuOriginal.getTxPopMenu();
        this.posicionPopMenu = nodoMenuOriginal.getPosicionPopMenu();
        this.tipoNodo = nodoMenuOriginal.getTipoNodo();
        this.icono = nodoMenuOriginal.getIcono();
    }

    /**
     * @return Identificador de la autoridad
     */
    public int getIdAutoridad() {
        return idAutoridad;
    }

    /**
     * @param idAutoridad
     *            Identificador de la autoridad
     */
    public void setIdAutoridad(int idAutoridad) {
        this.idAutoridad = idAutoridad;
    }

    /**
     * @return El recurso a proteger (URL, clase y/o m&eacute;todo JavaScript,
     *         clase y/o m&eacute;todo Java)
     */
    public String getDetalleAutoridad() {
        return detalleAutoridad;
    }

    /**
     * @param detalleAutoridad
     *            El recurso a proteger (URL, clase y/o m&eacute;todo
     *            JavaScript, clase y/o m&eacute;todo Java)
     */
    public void setDetalleAutoridad(String detalleAutoridad) {
        this.detalleAutoridad = detalleAutoridad;
    }

    /**
     * @return Posici&oacute;n en el &aacute;rbol del men&uacute; cuando no se
     *         construye recursivamente
     */
    public String getNivelPopMenu() {
        return nivelPopMenu;
    }

    /**
     * @param nivelPopMenu
     *            Posici&oacute;n en el &aacute;rbol del men&uacute; cuando no
     *            se construye recursivamente
     */
    public void setNivelPopMenu(String nivelPopMenu) {
        this.nivelPopMenu = nivelPopMenu;
    }

    /**
     * @return Identificador de la opci&oacute;n de men&uacute;. Cuando se
     *         construye recursivamente, es el identificador del nodo en el
     *         men&uacute;
     */
    public int getIdOpcMenu() {
        return idOpcMenu;
    }

    /**
     * @param idOpcMenu
     *            Identificador de la opci&oacute;n de men&uacute;. Cuando se
     *            construye recursivamente, es el identificador del nodo en el
     *            men&uacute;
     */
    public void setIdOpcMenu(int idOpcMenu) {
        this.idOpcMenu = idOpcMenu;
    }

    /**
     * @return Valor de HREF
     */
    public String getUrlPopMenu() {
        return urlPopMenu;
    }

    /**
     * @param urlPopMenu
     *            Valor de HREF
     */
    public void setUrlPopMenu(String urlPopMenu) {
        this.urlPopMenu = urlPopMenu;
    }

    /**
     * @return El nodo padre de ese elemento de men&uacute; para
     *         construcci&oacute;n recursiva
     */
    public int getGrupoRefPopMenu() {
        return grupoRefPopMenu;
    }

    /**
     * @param grupoRefPopMenu
     *            El nodo padre de ese elemento de men&uacute; para
     *            construcci&oacute;n recursiva
     */
    public void setGrupoRefPopMenu(int grupoRefPopMenu) {
        this.grupoRefPopMenu = grupoRefPopMenu;
    }

    /**
     * @return Etiqueta para la opci&oacute;n de men&uacute;
     */
    public String getTxPopMenu() {
        return txPopMenu;
    }

    /**
     * @param txPopMenu
     *            Etiqueta para la opci&oacute;n de men&uacute;
     */
    public void setTxPopMenu(String txPopMenu) {
        this.txPopMenu = txPopMenu;
    }

    /**
     * @return Posici&oacute;n a partir del nodo padre para construcci&oacute;n
     *         recursiva
     */
    public int getPosicionPopMenu() {
        return posicionPopMenu;
    }

    /**
     * @param posicionPopMenu
     *            Posici&oacute;n a partir del nodo padre para
     *            construcci&oacute;n recursiva
     */
    public void setPosicionPopMenu(int posicionPopMenu) {
        this.posicionPopMenu = posicionPopMenu;
    }

    /**
     * @return Estilo del nodo (S&oacute;lo puede ser 'R':ra&iacute;z o
     *         'O':opci&oacute;n)
     */
    public String getTipoNodo() {
        return tipoNodo;
    }

    /**
     * @param tipoNodo
     *            Estilo del nodo (S&oacute;lo puede ser 'R':ra&iacute;z o
     *            'O':opci&oacute;n)
     */
    public void setTipoNodo(String tipoNodo) {
        this.tipoNodo = tipoNodo;
    }

    /**
     * @return V&iacute;nculo a icono que se mostrar&aacute; en el nodo
     */
    public String getIcono() {
        return icono;
    }

    /**
     * @param icono
     *            V&iacute;nculo a icono que se mostrar&aacute; en el nodo
     */
    public void setIcono(String icono) {
        this.icono = icono;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NodoMenu [idAutoridad=" + idAutoridad + ", detalleAutoridad=" + detalleAutoridad + ", nivelPopMenu="
                + nivelPopMenu + ", idOpcMenu=" + idOpcMenu + ", urlPopMenu=" + urlPopMenu + ", grupoRefPopMenu="
                + grupoRefPopMenu + ", txPopMenu=" + txPopMenu + ", posicionPopMenu=" + posicionPopMenu + ", tipoNodo="
                + tipoNodo + ", icono=" + icono + "]";
    }
}
