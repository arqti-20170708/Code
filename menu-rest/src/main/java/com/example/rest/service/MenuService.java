package com.example.rest.service;

import com.example.rest.bean.EmpresaMenuCollection;
import com.example.rest.exception.ApiGeneralException;

/**
 * {@inheritDoc}
 * 
 * Interfaz en la capa de servicios Spring para la ejecuci&oacute;n de las
 * capacidades del servicio RESTful
 * {@link com.example.rest.controller.MenuController MenuController}
 * 
 */
public interface MenuService {

    /**
     * Obtiene la colecci&oacute;n (listado) de empresas con su men&uacute; para
     * un usuario de una aplicaci&oacute;n, cuyas claves son dadas c&oacute;mo
     * par&aacute;metros<br>
     * <br>
     * <i>La implementaci&oacute;n de este m&eacute;todo en la clase
     * {@link com.example.rest.service.impl.MenuServiceImpl} devuelve valores
     * de prueba</i>
     * 
     * @param cveAplicacion
     *            Clave de la aplicaci&oacute;n
     * @param cveUsuario
     *            Clave del usuario
     * @param cveEmpresa
     *            Clave de la empresa
     * @return Colecci&oacute;n (listado) de empresas con su men&uacute; para un
     *         usuario de una aplicaci&oacute;n, cuyas claves son dadas
     *         c&oacute;mo par&aacute;metros
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    EmpresaMenuCollection getEmpresaMenuCollection(String cveAplicacion, String cveUsuario, String cveEmpresa)
            throws ApiGeneralException;
}
