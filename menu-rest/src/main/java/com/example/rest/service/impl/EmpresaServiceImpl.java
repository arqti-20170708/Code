package com.example.rest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.rest.bean.Empresa;
import com.example.rest.bean.EmpresaCollection;
import com.example.rest.service.EmpresaService;
import com.example.rest.exception.ApiGeneralException;
import com.example.rest.exception.BadRequestStatusException;
import com.example.rest.exception.NotFoundStatusException;

/**
 * {@inheritDoc}
 * 
 * Implementaci&oacute;n en la capa de servicios Spring de la interface
 * {@link com.example.rest.service.EmpresaService EmpresaService} para la
 * ejecuci&oacute;n de las capacidades del servicio RESTful
 * {@link com.example.rest.controller.EmpresaController EmpresaController}
 * 
 */
@Service
public class EmpresaServiceImpl implements EmpresaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmpresaServiceImpl.class);

    /**
     * Obtiene la colecci&oacute;n (listado) de empresas<br>
     * <br>
     * <i>Esta implementaci&oacute;n devuelve valores de prueba</i>
     * 
     * @return Colecci&oacute;n (listado) de empresas
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    @Override
    public EmpresaCollection getEmpresaCollection() throws ApiGeneralException {
        // TODO: Simulación. Cambiar por la lógica de negocio que recupere esta
        // información del modelo de datos

        Empresa empresaMock01 = new Empresa("exampleMock1", "Empresa mock 1 para simulación");
        Empresa empresaMock02 = new Empresa("exampleMock2", "Empresa mock 2 para simulación");
        Empresa empresaMock03 = new Empresa("exampleMock3", "Empresa mock 3 para simulación");

        List<Empresa> listEmpresasMock01 = new ArrayList<Empresa>();
        listEmpresasMock01.add(empresaMock01);
        listEmpresasMock01.add(empresaMock02);
        listEmpresasMock01.add(empresaMock03);

        EmpresaCollection empresaCollectionMock01 = new EmpresaCollection(listEmpresasMock01);
        return empresaCollectionMock01;
    }

    /**
     * Obtiene una empresa a partir del valor de clave de empresa<br>
     * <br>
     * <i>Esta implementaci&oacute;n devuelve valores de prueba</i>
     * 
     * @param cveEmpresa
     *            Clave de la empresa
     * @return Empresa con clave de empresa que corresponda al par&aacute;metro
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    @Override
    public Empresa getEmpresaByCveEmpresa(String cveEmpresa) throws ApiGeneralException {
        // TODO: Simulación. Cambiar por la lógica de negocio que recupere esta
        // información del modelo de datos

        if (cveEmpresa.isEmpty()) {
            int referencia = ApiGeneralException.randInt();
            String description = "No se ha enviado clave";
            String info = "[cveEmpresa nulo o cadena vacía]";
            String message = "Verificar con el administrador el historial del servicio con la referencia " + referencia;
            BadRequestStatusException exception = new BadRequestStatusException(referencia, description, info, message);
            LOGGER.error(exception.toString());
            throw exception;
        }

        if (cveEmpresa.equals("cveNotFound")) {
            int referencia = ApiGeneralException.randInt();
            String description = "No se ha encontrado una empresa con la clave introducida";
            String info = "[cveEmpresa: '" + cveEmpresa + "']";
            String message = "Verificar con el administrador el historial del servicio con la referencia " + referencia;
            NotFoundStatusException exception = new NotFoundStatusException(referencia, description, info, message);
            LOGGER.error(exception.toString());
            throw exception;
        }

        Empresa empresaMock01 = new Empresa(cveEmpresa, "Empresa mock para simulación");

        return empresaMock01;
    }
}
