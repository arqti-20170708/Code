package com.example.rest.service;

import com.example.rest.bean.Empresa;
import com.example.rest.bean.EmpresaCollection;
import com.example.rest.exception.ApiGeneralException;

/**
 * {@inheritDoc}
 * 
 * Interfaz en la capa de servicios Spring para la ejecuci&oacute;n de las
 * capacidades del servicio RESTful
 * {@link com.example.rest.controller.EmpresaController EmpresaController}
 * 
 */
public interface EmpresaService {

    /**
     * Obtiene la colecci&oacute;n (listado) de empresas<br>
     * <br>
     * <i>La implementaci&oacute;n de este m&eacute;todo en la clase
     * {@link com.example.rest.service.impl.MenuServiceImpl} devuelve valores
     * de prueba</i>
     * 
     * @return Colecci&oacute;n (listado) de empresas
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    EmpresaCollection getEmpresaCollection() throws ApiGeneralException;

    /**
     * Obtiene una empresa a partir del valor de clave de empresa<br>
     * <br>
     * <i>La implementaci&oacute;n de este m&eacute;todo en la clase
     * {@link com.example.rest.service.impl.MenuServiceImpl} devuelve valores
     * de prueba</i>
     * 
     * @param cveEmpresa
     *            Clave de la empresa
     * @return Empresa con clave de empresa que corresponda al par&aacute;metro
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    Empresa getEmpresaByCveEmpresa(String cveEmpresa) throws ApiGeneralException;
}
