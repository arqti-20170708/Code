package com.example.rest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.rest.bean.EmpresaMenu;
import com.example.rest.bean.EmpresaMenuCollection;
import com.example.rest.bean.Menu;
import com.example.rest.bean.NodoMenu;
import com.example.rest.service.MenuService;
import com.example.rest.exception.ApiGeneralException;
import com.example.rest.exception.NotFoundStatusException;

/**
 * {@inheritDoc}
 * 
 * Implementaci&oacute;n en la capa de servicios Spring de la interface
 * {@link com.example.rest.service.MenuService MenuService} para la
 * ejecuci&oacute;n de las capacidades del servicio RESTful
 * {@link com.example.rest.controller.MenuController MenuController}
 * 
 */
@Service
public class MenuServiceImpl implements MenuService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MenuServiceImpl.class);

    /**
     * Obtiene la colecci&oacute;n (listado) de empresas con su men&uacute; para
     * un usuario de una aplicaci&oacute;n, cuyas claves son dadas c&oacute;mo
     * par&aacute;metros<br>
     * <br>
     * <i>Esta implementaci&oacute;n devuelve valores de prueba</i>
     * 
     * @param cveAplicacion
     *            Clave de la aplicaci&oacute;n
     * @param cveUsuario
     *            Clave del usuario
     * @param cveEmpresa
     *            Clave de la empresa
     * @return Colecci&oacute;n (listado) de empresas con su men&uacute; para un
     *         usuario de una aplicaci&oacute;n, cuyas claves son dadas
     *         c&oacute;mo par&aacute;metros
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    @Override
    public EmpresaMenuCollection getEmpresaMenuCollection(String cveAplicacion, String cveUsuario, String cveEmpresa)
            throws ApiGeneralException {
        // TODO: Simulación. Cambiar por la lógica de negocio que recupere esta
        // información del modelo de datos utilizando los valores de
        // 'cveAplicacion' y 'cveUsuario'

        if (cveEmpresa != null && cveEmpresa.equals("cveNotFound")) {
            int referencia = ApiGeneralException.randInt();
            String description = "No se ha encontrado una empresa con la clave introducida";
            String info = "[cveEmpresa: '" + cveEmpresa + "']";
            String message = "Verificar con el administrador el historial del servicio con la referencia " + referencia;
            NotFoundStatusException exception = new NotFoundStatusException(referencia, description, info, message);
            LOGGER.error(exception.toString());
            throw exception;
        }

        NodoMenu nodoMock01 = new NodoMenu(2515, "!/tesoreria/notificar-depositos", "1", 313, "#", 0,
                "Depósitos y Retiros de  Dinero (TESORERIA)", 1, "R",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock02 = new NodoMenu(2515, "!/tesoreria/notificar-depositos", "1.1", 314,
                "!/tesoreria/notificar-depositos", 313, "Notificar Depósitos", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock03 = new NodoMenu(2516, "!/tesoreria/ordenar-retiros", "1.2", 315,
                "!/tesoreria/ordenar-retiros", 313, "Ordenar Retiros", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock04 = new NodoMenu(2517, "!/tesoreria/consultas", "1.3", 316, "!/tesoreria/consultas", 313,
                "Consultar Depósitos y retiros", 2, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock05 = new NodoMenu(2520, "!/tesoreria/cancelacion", "1.6", 319, "!/tesoreria/cancelacion", 313,
                "o   cancelación de movimientos", 5, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock06 = new NodoMenu(2522, "!/consult/movement", "2", 320, "#", 0, "Consultas", 2, "R",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock07 = new NodoMenu(2522, "!/consult/movement", "2.1", 321, "!/consult/movement", 320,
                "Saldos y movimientos", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock08 = new NodoMenu(2523, "!/consult/statementt", "2.2", 322, "!/consult/statement", 320,
                "Estado de cuenta", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock09 = new NodoMenu(2524, "!/consult/movement-extraction", "2.3", 323,
                "!/consult/movement-extraction", 320, "Extracción de movimientos", 2, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock10 = new NodoMenu(2526, "!/casabolsa/posicionFinanciera", "3", 324, "#", 0,
                "Consultas Casa de Bolsa", 3, "R",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock11 = new NodoMenu(2526, "!/casabolsa/posicionFinanciera", "3.1", 325,
                "!/casabolsa/posicionFinanciera", 324, "Posición", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock12 = new NodoMenu(2527, "!/casabolsa/movimientos", "3.2", 326, "!/casabolsa/movimientos", 324,
                "Movimientos", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock13 = new NodoMenu(2528, "!/casabolsa/portafolio", "3.3", 327, "!/casabolsa/portafolio", 324,
                "Portafolio", 2, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock14 = new NodoMenu(2529, "!/casabolsa/resumen", "3.4", 328, "!/casabolsa/resumen", 324,
                "Resumen", 3, "O", "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock15 = new NodoMenu(2530, "!/casabolsa/impuestos", "3.5", 329, "!/casabolsa/impuestos", 324,
                "Impuestos", 4, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock16 = new NodoMenu(2531, "!/consult/statement", "3.6", 330, "!/consult/statement", 324,
                "Estado de cuenta", 5, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock17 = new NodoMenu(2534, "!//userData/cambio-contrasenas", "4.2", 333,
                "!/userData/cambio-contrasenas", 331, "Cambio de contraseñas", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock18 = new NodoMenu(2535, "!/misdatos/user-mail", "4", 331, "#", 0, "Mis datos", 4, "R",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock19 = new NodoMenu(2535, "!/misdatos/user-mail", "4.3", 334, "!/misdatos/user-mail", 331,
                "Alta y Cambio de Email", 2, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock20 = new NodoMenu(2536, "!/misdatos/actualiza_correo_notificaciones", "4.4", 335,
                "!/misdatos/actualiza_correo_notificaciones", 331, "Actualización de E-mail de notificaciones", 3, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock21 = new NodoMenu(2538, "!/misdatos/act-perfil-transaccional", "4.6", 337,
                "!/misdatos/act-perfil-transaccional", 331, "Actualización de perfil Transaccional", 5, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock22 = new NodoMenu(2539, "!/misdatos/administrar-cuentas", "4.8", 338,
                "!/misdatos/administrar-cuentas", 331, "Cuentas Registradas", 7, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock23 = new NodoMenu(2540, "!/misdatos/registro-cuentas", "4.7", 339,
                "!/misdatos/registro-cuentas", 331, "Nuevo Registro de Cuenta", 6, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock24 = new NodoMenu(2542, "!/pagoserv/registro", "5", 340, "#", 0, "Pago de servicios", 5, "R",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock25 = new NodoMenu(2542, "!/pagoserv/registro", "5.3", 341, "!/pagoserv/registro", 340,
                "Nuevo Registro de Servicios", 2, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock26 = new NodoMenu(2543, "!/pagoserv/consultapagoserv", "5.4", 342,
                "!/pagoserv/consultapagoserv", 340, "Servicios registrados", 3, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock27 = new NodoMenu(2544, "!/pagoserv/pagoserv", "5.1", 343, "!/pagoserv/pagoserv", 340,
                "Pago Telmex", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock28 = new NodoMenu(2546, "!/prlv/consultaPRLV", "6", 344, "#", 0, "Inversion PRLV", 6, "R",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock29 = new NodoMenu(2546, "!/prlv/consultaPRLV", "6.1", 345, "!/prlv/consultaPRLV", 344,
                "Detalle Inversión PRLV", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock30 = new NodoMenu(2547, "!/prlv/edoCuentaPRLV", "6.2", 346, "!/prlv/edoCuentaPRLV", 344,
                "Estado Cuenta PRLV", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock31 = new NodoMenu(2549, "!/capitales/compraventa", "7", 347, "#", 0, "Mercado Capitales", 7,
                "R", "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock32 = new NodoMenu(2549, "!/capitales/compraventa", "7.1", 348, "!/capitales/compraventa", 347,
                "Compra o Venta", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock33 = new NodoMenu(2550, "!/capitales/consulta", "7.2", 349, "!/capitales/consulta", 347,
                "Consulta y Modificación de Ordenes", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock34 = new NodoMenu(2552, "!/tesofe/pago", "8", 350, "#", 0, "Pagos Línea de Captura TESOFE", 8,
                "R", "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock35 = new NodoMenu(2552, "!/tesofe/pago", "8.1", 351, "!/tesofe/pago", 350,
                "Pago Línea de Captura", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock36 = new NodoMenu(2553, "!/tesofe/consultapago", "8.2", 352, "!/tesofe/consultapago", 350,
                "Consulta de Pagos", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock37 = new NodoMenu(2555, "!/sdi/comprasdi", "9", 353, "#", 0, "Fondos de Inversión", 9, "R",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock38 = new NodoMenu(2555, "!/sdi/comprasdi", "9.1", 354, "!/sdi/comprasdi", 353,
                "Compra o Venta", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock39 = new NodoMenu(2556, "!/sdi/consultasdi", "9.2", 355, "!/sdi/consultasdi", 353, "Consulta",
                1, "O", "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock40 = new NodoMenu(2558, "!/pagotdc/registro", "10", 356, "#", 0, "Tarjeta de Crédito", 10, "R",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock41 = new NodoMenu(2558, "!/pagotdc/registro", "10.2", 357, "!/pagotdc/registro", 356,
                "Nuevo registro de TDC", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock42 = new NodoMenu(2559, "!/pagotdc/consultatdc", "10.3", 358, "!/pagotdc/consultatdc", 356,
                "Consulta TDC registradas", 2, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock43 = new NodoMenu(2560, "!/pagotdc/pagotdc", "10.1", 359, "!/pagotdc/pagotdc", 356,
                "Pago de TDC", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock44 = new NodoMenu(2562, "!/admin/consulta-chequera", "11", 360, "#", 0, "Administración", 11,
                "R", "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock45 = new NodoMenu(2562, "!/admin/consulta-chequera", "11.1", 361, "!/admin/consulta-chequera",
                360, "Solicitud de chequera", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock46 = new NodoMenu(2564, "!/transferencias/mismobanco", "12", 362, "#", 0, "Transferencias", 12,
                "R", "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock47 = new NodoMenu(2564, "!/transferencias/mismobanco", "12.2", 363,
                "!/transferencias/mismobanco", 362, "Traspaso Mismo Banco", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock48 = new NodoMenu(2565, "!/transferencias/spei", "12.1", 364, "!/transferencias/spei", 362,
                "Transferencias otros bancos", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock49 = new NodoMenu(2566, "!/transferencias/consultaspei", "12.3", 365,
                "!/transferencias/consultaspei", 362, "Consulta de movimientos SPEI", 2, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock50 = new NodoMenu(2568, "!/pagoimpuesto/pagopda", "13", 366, "#", 0, "Pago de Impuestos", 13,
                "R", "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock51 = new NodoMenu(2568, "!/pagoimpuesto/pagopda", "13.2", 367, "!/pagoimpuesto/pagopda", 366,
                "Pago de Derechos, Productos y Aprovechamientos", 1, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock52 = new NodoMenu(2569, "!/pagoimpuesto/consultapda", "13.1", 368,
                "!/pagoimpuesto/consultapda", 366, "Consulta de Pagos", 0, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock53 = new NodoMenu(2665, "!/transferencias/masivaarchivo", "12.5", 372,
                "!/transferencias/masivaarchivo", 362, "Transferencia por Archivo", 4, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock54 = new NodoMenu(2666, "!/transferencias/capturaspei", "12.4", 373,
                "!/transferencias/capturaspei", 362, "Transferencia por Grupo", 3, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock55 = new NodoMenu(2667, "!/transferencias/consulta", "12.6", 374, "!/transferencias/consulta",
                362, "Autoriza/Consulta Transferencias", 5, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock56 = new NodoMenu(2685, "!/pagoimpuesto/pagorefpda", "13.3", 375, "!/pagoimpuesto/pagorefpda",
                366, "Pago Referenciado SAT", 2, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock57 = new NodoMenu(14116, "!/cuentas-archivo", "4.10", 1624, "!/cuentas-archivo", 331,
                "Alta de cuentas por archivo", 10, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock58 = new NodoMenu(14167, "!/consulta-usuarios", "11.4", 1630, "!/consulta-usuarios", 360,
                "Consulta de usuarios", 4, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");
        NodoMenu nodoMock59 = new NodoMenu(14207, "!/portabilidad-nomina", "11.5", 1647, "!/portabilidad-nomina", 360,
                "Portabilidad de nómina", 5, "O",
                "http://www.exaple.com.mx/templates/example/favicon.ico");

        List<NodoMenu> nodosMock01 = new ArrayList<NodoMenu>();
        nodosMock01.add(nodoMock01);
        nodosMock01.add(nodoMock02);
        nodosMock01.add(nodoMock03);
        nodosMock01.add(nodoMock04);
        nodosMock01.add(nodoMock05);
        nodosMock01.add(nodoMock06);
        nodosMock01.add(nodoMock07);
        nodosMock01.add(nodoMock08);
        nodosMock01.add(nodoMock09);
        nodosMock01.add(nodoMock10);
        nodosMock01.add(nodoMock11);
        nodosMock01.add(nodoMock12);
        nodosMock01.add(nodoMock13);
        nodosMock01.add(nodoMock14);
        nodosMock01.add(nodoMock15);
        nodosMock01.add(nodoMock16);
        nodosMock01.add(nodoMock17);
        nodosMock01.add(nodoMock18);
        nodosMock01.add(nodoMock19);
        nodosMock01.add(nodoMock20);
        nodosMock01.add(nodoMock21);
        nodosMock01.add(nodoMock22);
        nodosMock01.add(nodoMock23);
        nodosMock01.add(nodoMock24);
        nodosMock01.add(nodoMock25);
        nodosMock01.add(nodoMock26);
        nodosMock01.add(nodoMock27);
        nodosMock01.add(nodoMock28);
        nodosMock01.add(nodoMock29);
        nodosMock01.add(nodoMock30);
        nodosMock01.add(nodoMock31);
        nodosMock01.add(nodoMock32);
        nodosMock01.add(nodoMock33);
        nodosMock01.add(nodoMock34);
        nodosMock01.add(nodoMock35);
        nodosMock01.add(nodoMock36);
        nodosMock01.add(nodoMock37);
        nodosMock01.add(nodoMock38);
        nodosMock01.add(nodoMock39);
        nodosMock01.add(nodoMock40);
        nodosMock01.add(nodoMock41);
        nodosMock01.add(nodoMock42);
        nodosMock01.add(nodoMock43);
        nodosMock01.add(nodoMock44);
        nodosMock01.add(nodoMock45);
        nodosMock01.add(nodoMock46);
        nodosMock01.add(nodoMock47);
        nodosMock01.add(nodoMock48);
        nodosMock01.add(nodoMock49);
        nodosMock01.add(nodoMock50);
        nodosMock01.add(nodoMock51);
        nodosMock01.add(nodoMock52);
        nodosMock01.add(nodoMock53);
        nodosMock01.add(nodoMock54);
        nodosMock01.add(nodoMock55);
        nodosMock01.add(nodoMock56);
        nodosMock01.add(nodoMock57);
        nodosMock01.add(nodoMock58);
        nodosMock01.add(nodoMock59);

        Menu menuMock01 = new Menu(nodosMock01);

        EmpresaMenuCollection empresaMenuCollectionMock01 = new EmpresaMenuCollection();

        if (cveEmpresa == null || cveEmpresa.equals("") || cveEmpresa.isEmpty()) {
            // Mostrar menú de todas las empresas
            EmpresaMenu empresaMock01 = new EmpresaMenu("exampleMock1", "Empresa mock 1 para simulación", menuMock01);
            EmpresaMenu empresaMock02 = new EmpresaMenu("exampleMock3", "Empresa mock 2 para simulación", menuMock01);
            EmpresaMenu empresaMock03 = new EmpresaMenu("exampleMock2", "Empresa mock 3 para simulación", menuMock01);

            List<EmpresaMenu> listEmpresasMock01 = new ArrayList<EmpresaMenu>();
            listEmpresasMock01.add(empresaMock01);
            listEmpresasMock01.add(empresaMock02);
            listEmpresasMock01.add(empresaMock03);

            empresaMenuCollectionMock01.setEmpresas(listEmpresasMock01);
        } else {
            // Mostrar menú de la empresa
            EmpresaMenu empresaMock01 = new EmpresaMenu(cveEmpresa, "Empresa mock para simulación", menuMock01);

            List<EmpresaMenu> listEmpresasMock01 = new ArrayList<EmpresaMenu>();
            listEmpresasMock01.add(empresaMock01);

            empresaMenuCollectionMock01.setEmpresas(listEmpresasMock01);

        }

        return empresaMenuCollectionMock01;
    }
}
