package com.example.rest.configuration.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada para configurar la UI del marco swagger-springmvc. Se puede
 * acceder a la documentaci&oacute;n proporcionada por SwaggerUI desde:
 * {scheme}://{hostname}:{port}/menu-rest/swagger-ui.html
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    private static final String API_TITLE = "Título de la API";
    private static final String API_DESCRIPTION = "Descripción de la API";
    private static final String API_VERSION = "2.0";
    private static final String API_TERMS_OF_SERVICE_URL = "http://www.example.com.mx/";
    private static final String API_CONTACT_NAME = "Contacto";
    private static final String API_LICENSE = "Licencia de la API";
    private static final String API_LICENSE_URL = "http://www.example.com.mx/";

    /**
     * @return Objecto del constructor destinado a ser la interfaz principal del
     *         marco swagger-springmvc. Proporcionar&aacute; los valores
     *         predeterminados y los métodos convenientes para la
     *         configuraci&oacute;n.
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any()).build().apiInfo(apiInfo());
    }

    // Internal helpers
    @SuppressWarnings("deprecation")
    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(API_TITLE, API_DESCRIPTION, API_VERSION, API_TERMS_OF_SERVICE_URL,
                API_CONTACT_NAME, API_LICENSE, API_LICENSE_URL);
        return apiInfo;
    }
}
