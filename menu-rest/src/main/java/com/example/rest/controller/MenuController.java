package com.example.rest.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.rest.bean.EmpresaMenu;
import com.example.rest.bean.EmpresaMenuCollection;
import com.example.rest.bean.NodoMenu;
import com.example.rest.service.MenuService;
import com.example.rest.exception.ApiGeneralException;

/**
 * {@inheritDoc}
 * 
 * Controlador del servicio RESTful para obtener informaci&oacute;n relacionada
 * con el men&uacute;, ordenadas por empresa
 * 
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodoMenu.class);
    private static final String API_VERSION = "1.0";

    @Autowired
    private MenuService menuService;

    /**
     * Obtiene la colecci&oacute;n (listado) de empresas con su men&uacute; para
     * un usuario de una aplicaci&oacute;n, cuyas claves son dadas c&oacute;mo
     * par&aacute;metros
     * 
     * @param cveAplicacion
     *            Clave de la aplicaci&oacute;n. Par&aacute;metro requerido en
     *            la llamada al recurso
     * @param cveUsuario
     *            Clave del usuario. Par&aacute;metro requerido en la llamada al
     *            recurso
     * @param cveEmpresa
     *            Clave de la empresa. Par&aacute;metro opcional en la llamada
     *            al recurso
     * @return Colecci&oacute;n (listado) de empresas con su men&uacute; para un
     *         usuario de una aplicaci&oacute;n, cuyas claves son dadas
     *         c&oacute;mo par&aacute;metros
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    @RequestMapping(value = "/v"
            + API_VERSION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody EmpresaMenuCollection getEmpresaMenuCollection(
            @RequestParam(value = "cve-aplicacion", required = true) String cveAplicacion,
            @RequestParam(value = "cve-usuario", required = true) String cveUsuario,
            @RequestParam(value = "cve-empresa", required = false) String cveEmpresa) throws ApiGeneralException {
        LOGGER.debug("'GET' de un 'empresa' por 'cveEmpresa'");
        EmpresaMenuCollection empresaMenuCollection = menuService.getEmpresaMenuCollection(cveAplicacion, cveUsuario,
                cveEmpresa);
        LOGGER.debug("Se ha salido del método 'getEmpresaMenuCollection' del 'menuService'");

        // Agregar links HATEOAS
        empresaMenuCollection = this.addLinksToCollection(empresaMenuCollection);
        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(MenuController.class).getEmpresaMenuCollection(cveAplicacion, cveUsuario, cveEmpresa))
                .withSelfRel());
        empresaMenuCollection.add(links);

        return empresaMenuCollection;
    }

    // Internal helpers
    private EmpresaMenuCollection addLinksToCollection(EmpresaMenuCollection empresaMenuCollectionOriginal)
            throws ApiGeneralException {
        List<EmpresaMenu> listEmpresasOriginal = empresaMenuCollectionOriginal.getEmpresas();
        List<EmpresaMenu> listEmpresas = new ArrayList<EmpresaMenu>();

        for (EmpresaMenu empresaMock : listEmpresasOriginal) {
            List<Link> linksEmpresaMock = new ArrayList<Link>();
            linksEmpresaMock
                    .add(linkTo(methodOn(EmpresaController.class).getEmpresaByCveEmpresa(empresaMock.getCveEmpresa()))
                            .withRel("empresa"));
            empresaMock.add(linksEmpresaMock);

            listEmpresas.add(empresaMock);
        }
        return new EmpresaMenuCollection(listEmpresas);
    }
}
