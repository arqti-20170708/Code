package com.example.rest.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.rest.bean.Empresa;
import com.example.rest.bean.EmpresaCollection;
import com.example.rest.service.EmpresaService;
import com.example.rest.exception.ApiGeneralException;

/**
 * {@inheritDoc}
 * 
 * Controlador del servicio RESTful para obtener informaci&oacute;n relacionada
 * con las empresas
 * 
 */
@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmpresaController.class);
    private static final String API_VERSION = "1.0";

    @Autowired
    private EmpresaService empresaService;

    /**
     * Obtiene la colecci&oacute;n (listado) de empresas
     * 
     * @return Colecci&oacute;n (listado) de empresas
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    @RequestMapping(value = "/coleccion/v"
            + API_VERSION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody EmpresaCollection getEmpresaCollection() throws ApiGeneralException {
        LOGGER.debug("'GET' de la colección de empresas");
        EmpresaCollection empresaCollection = empresaService.getEmpresaCollection();
        LOGGER.debug("Se ha salido del método 'getEmpresaCollection' del 'empresaService'");

        // Agregar links HATEOAS
        empresaCollection = this.addLinksToCollection(empresaCollection);
        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(EmpresaController.class).getEmpresaCollection()).withSelfRel());
        empresaCollection.add(links);

        return empresaCollection;
    }

    /**
     * Obtiene una empresa a partir del valor de clave de empresa
     * 
     * @param cveEmpresa
     *            Clave de la empresa. Par&aacute;metro requerido en la llamada
     *            al recurso
     * @return Empresa con clave de empresa que corresponda al par&aacute;metro
     * @throws ApiGeneralException
     *             Excepci&oacute;n de la que extienden todas aquellas que
     *             pueden ser lanzadas durante la ejecuci&oacute;n de la
     *             l&oacute;gica de negocio que recupera la informaci&oacute;n
     *             del modelo de datos
     */
    @RequestMapping(value = "/v"
            + API_VERSION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody Empresa getEmpresaByCveEmpresa(
            @RequestParam(value = "cve-empresa", required = true) String cveEmpresa) throws ApiGeneralException {
        LOGGER.debug("'GET' de un 'empresa' por 'cveEmpresa'");
        Empresa empresa = empresaService.getEmpresaByCveEmpresa(cveEmpresa);
        LOGGER.debug("Se ha salido del método 'getEmpresaByCveEmpresa' del 'empresaService'");

        // Agregar links HATEOAS
        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(EmpresaController.class).getEmpresaCollection()).withRel("coleccion"));
        links.add(linkTo(methodOn(EmpresaController.class).getEmpresaByCveEmpresa(cveEmpresa)).withSelfRel());
        empresa.add(links);

        return empresa;
    }

    // Internal helpers
    private EmpresaCollection addLinksToCollection(EmpresaCollection empresaCollectionOriginal)
            throws ApiGeneralException {
        List<Empresa> listEmpresasOriginal = empresaCollectionOriginal.getEmpresas();
        List<Empresa> listEmpresas = new ArrayList<Empresa>();

        for (Empresa empresaMock : listEmpresasOriginal) {
            List<Link> linksEmpresaMock = new ArrayList<Link>();
            linksEmpresaMock.add(linkTo(methodOn(com.example.rest.controller.EmpresaController.class)
                    .getEmpresaByCveEmpresa(empresaMock.getCveEmpresa())).withSelfRel());
            empresaMock.add(linksEmpresaMock);

            listEmpresas.add(empresaMock);
        }
        return new EmpresaCollection(listEmpresas);
    }
}
