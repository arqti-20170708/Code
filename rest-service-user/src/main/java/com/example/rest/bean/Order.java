package com.example.rest.bean;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada como bean por el modelo para la orden
 */
public class Order extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = -6859399399798770474L;

    private String orderId;
    private double price;
    private int quantity;

    /**
     * Constructor por defecto
     */
    public Order() {
        super();
        this.orderId = "";
        this.price = 0;
        this.quantity = 0;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param orderId
     *            La clave de la orden
     * @param price
     *            El precio de la orden
     * @param quantity
     *            La cantidad de objetos en la orden
     */
    public Order(String orderId, double price, int quantity) {
        super();
        this.orderId = orderId;
        this.price = price;
        this.quantity = quantity;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param orderOriginal
     *            Order original del que se copiarán los atributos
     */
    public Order(Order orderOriginal) {
        super();
        this.orderId = orderOriginal.getOrderId();
        this.price = orderOriginal.getPrice();
        this.quantity = orderOriginal.quantity;
    }

    /**
     * @return La clave de la orden
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     *            La clave de la orden
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return El precio de la orden
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price
     *            El precio de la orden
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return La cantidad de objetos en la orden
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            La cantidad de objetos en la orden
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Order [orderId=" + orderId + ", price=" + price + ", quantity=" + quantity + "]";
    }
}
