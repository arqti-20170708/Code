package com.example.rest.bean;

import java.io.Serializable;

import org.springframework.hateoas.ResourceSupport;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada como bean por el modelo para el usuario
 */
public class User extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = 8731656165875193533L;

    private String username;
    private String email;
    private String employeeId;

    /**
     * Constructor por defecto
     */
    public User() {
        super();
        this.username = "";
        this.email = "";
        this.employeeId = "";
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param username
     *            El nombre del usuario
     * @param email
     *            La direcci&oacute;n de correo electr&oacute;nico del usuario
     * @param employeeId
     *            La clave de empleado del usuario
     */
    public User(String username, String email, String employeeId) {
        super();
        this.username = username;
        this.email = email;
        this.employeeId = employeeId;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param userOriginal
     *            User original del que se copiarán los atributos
     */
    public User(User userOriginal) {
        super();
        this.username = userOriginal.getUsername();
        this.email = userOriginal.getEmail();
        this.employeeId = userOriginal.getEmployeeId();
    }

    /**
     * @return El nombre del usuario
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            El nombre del usuario
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return La direcci&oacute;n de correo electr&oacute;nico del usuario
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            La direcci&oacute;n de correo electr&oacute;nico del usuario
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return La clave de empleado del usuario
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId
     *            La clave de empleado del usuario
     */
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "User [username=" + username + ", email=" + email + ", employeeId=" + employeeId + "]";
    }
}