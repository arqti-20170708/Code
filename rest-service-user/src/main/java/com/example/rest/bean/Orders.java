package com.example.rest.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada para englobar un listado de ordenes a partir del modelo
 */
public class Orders extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = -3805180700033350379L;

    private List<Order> orders;

    /**
     * Constructor por defecto
     */
    public Orders() {
        super();
        this.orders = new ArrayList<Order>();
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param orders
     *            Lista de ordenes
     */
    public Orders(List<Order> orders) {
        super();
        this.orders = orders;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param ordersOriginal
     *            Orders original del que se copiarán los atributos
     */
    public Orders(Orders ordersOriginal) {
        super();
        this.orders = ordersOriginal.getOrders();
    }

    /**
     * @return Lista de ordenes
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * @param orders
     *            Lista de ordenes
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Orders [orders=" + orders + "]";
    }
}
