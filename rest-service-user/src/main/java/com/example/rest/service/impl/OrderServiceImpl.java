package com.example.rest.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.rest.bean.Order;
import com.example.rest.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Override
    public Order createOrder(Order order) {
        LOGGER.debug("Se simula la creación de la orden: " + order.toString());
        return order;
    }

    @Override
    public Order readOrder(String orderId) {
        Order order = new Order(orderId, 100.10, 10);

        LOGGER.debug("Se simula la obtención de la orden: " + order.toString());
        return order;
    }

    @Override
    public Order updateOrder(Order modificacionOrder, String orderId) {
        Order orderOriginal = new Order(orderId, 100.10, 10);

        Order orderModificado = new Order(orderOriginal);
        orderModificado.setOrderId(modificacionOrder.getOrderId());
        orderModificado.setPrice(modificacionOrder.getPrice());
        orderModificado.setQuantity(modificacionOrder.getQuantity());

        LOGGER.debug("Se simula la modificación de la orden: " + orderOriginal.toString() + " --> "
                + orderModificado.toString());
        return orderModificado;
    }

    @Override
    public Order deleteOrder(Order orderEliminar) {

        LOGGER.debug("Se simula el borrado de la orden: " + orderEliminar.toString());
        return orderEliminar;
    }
}
