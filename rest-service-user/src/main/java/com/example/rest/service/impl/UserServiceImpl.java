package com.example.rest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.rest.bean.Order;
import com.example.rest.bean.User;
import com.example.rest.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public User createUser(User user) {
        LOGGER.debug("Se simula la creación del usuario: " + user.toString());
        return user;
    }

    @Override
    public User readUser(String employeeId) {
        User user = new User("username", "usuario@servidor.com", employeeId);

        LOGGER.debug("Se simula la obtención del usuario: " + user.toString());
        return user;
    }

    @Override
    public User updateUser(User modificacionUser, String employeeId) {
        User userOriginal = new User("username", "usuario@servidor.com", employeeId);

        User userModificado = new User(userOriginal);
        userModificado.setUsername(modificacionUser.getUsername());
        userModificado.setEmail(modificacionUser.getEmail());
        userModificado.setEmployeeId(modificacionUser.getEmployeeId());

        LOGGER.debug("Se simula la modificación del usuario: " + userOriginal.toString() + " --> "
                + userModificado.toString());
        return userModificado;
    }

    @Override
    public User deleteUser(User userEliminar) {

        LOGGER.debug("Se simula el borrado del usuario: " + userEliminar.toString());
        return userEliminar;
    }

    @Override
    public List<Order> getAllOrders(String employeeId) {
        List<Order> allOrders = new ArrayList<Order>();

        allOrders.add(new Order("ord01", 100.10, 10));
        allOrders.add(new Order("ord02", 200.20, 20));
        allOrders.add(new Order("ord03", 300.30, 30));

        LOGGER.debug("Se simila la obtención de todas las ordenes para el usuario con employeeId: " + employeeId);
        return allOrders;
    }
}
