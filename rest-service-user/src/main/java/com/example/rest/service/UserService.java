package com.example.rest.service;

import java.util.List;

import com.example.rest.bean.Order;
import com.example.rest.bean.User;

/**
 * Interfaz para la ejecuci&oacute;n de las capacidades del servicio User
 */
public interface UserService {

    User createUser(User user);

    User readUser(String employeeId);

    User updateUser(User modificacionUser, String employeeId);

    User deleteUser(User userEliminar);

    List<Order> getAllOrders(String employeeId);
}
