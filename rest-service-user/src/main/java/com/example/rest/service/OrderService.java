package com.example.rest.service;

import com.example.rest.bean.Order;

/**
 * Interfaz para la ejecuci&oacute;n de las capacidades del servicio Order
 */
public interface OrderService {

    Order createOrder(Order order);

    Order readOrder(String orderId);

    Order updateOrder(Order modificacionOrder, String orderId);

    Order deleteOrder(Order orderEliminar);
}
