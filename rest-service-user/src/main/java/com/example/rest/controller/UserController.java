package com.example.rest.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.rest.bean.Order;
import com.example.rest.bean.Orders;
import com.example.rest.bean.User;
import com.example.rest.service.UserService;
import com.example.rest.exception.ApiGeneralException;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final String API_VERSION = "2.0";

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/v" + API_VERSION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User getUserRequestParam(@RequestParam(value = "employee", required = true) String employeeId)
            throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'lectura' de un usuario");
        User user = userService.readUser(employeeId);

        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(UserController.class).getUserRequestParam(user.getEmployeeId())).withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getUserPathVariable(user.getEmployeeId())).withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getAllOrdersRequestParam(user.getEmployeeId()))
                .withRel("allOrders"));
        user.add(links);

        LOGGER.debug("Se ha salido del método 'readUser' del 'userService'");
        return user;
    }

    @RequestMapping(value = "/{employeeId}/v"
            + API_VERSION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User getUserPathVariable(@PathVariable String employeeId) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'lectura' de un usuario");
        User user = userService.readUser(employeeId);

        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(UserController.class).getUserRequestParam(user.getEmployeeId())).withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getUserPathVariable(user.getEmployeeId())).withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getAllOrdersRequestParam(user.getEmployeeId()))
                .withRel("allOrders"));
        user.add(links);

        LOGGER.debug("Se ha salido del método 'readUser' del 'userService'");
        return user;
    }

    @RequestMapping(value = "/v"
            + API_VERSION, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User createUser(@RequestBody User nuevoUser) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'creación' de un usuario");
        User user = userService.createUser(nuevoUser);

        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(UserController.class).getUserRequestParam(user.getEmployeeId())).withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getUserPathVariable(user.getEmployeeId())).withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getAllOrdersRequestParam(user.getEmployeeId()))
                .withRel("allOrders"));
        user.add(links);

        LOGGER.debug("Se ha salido del método 'createUser' del 'userService'");
        return user;
    }

    @RequestMapping(value = "/v" + API_VERSION, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User updateUserRequestParam(@RequestBody User modificacionUser,
            @RequestParam(value = "employee", required = true) String employeeId) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'actualización' de un usuario");
        User user = userService.updateUser(modificacionUser, employeeId);

        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(UserController.class).updateUserRequestParam(modificacionUser, user.getEmployeeId()))
                .withSelfRel());
        links.add(linkTo(methodOn(UserController.class).updateUserPathVariable(modificacionUser, user.getEmployeeId()))
                .withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getAllOrdersRequestParam(modificacionUser.getEmployeeId()))
                .withRel("allOrders"));
        user.add(links);

        LOGGER.debug("Se ha salido del método 'updateUser' del 'userService'");
        return user;
    }

    @RequestMapping(value = "/{employeeId}/v"
            + API_VERSION, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User updateUserPathVariable(@RequestBody User modificacionUser,
            @PathVariable String employeeId) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'actualización' de un usuario");
        User user = userService.updateUser(modificacionUser, employeeId);

        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(UserController.class).updateUserRequestParam(modificacionUser, user.getEmployeeId()))
                .withSelfRel());
        links.add(linkTo(methodOn(UserController.class).updateUserPathVariable(modificacionUser, user.getEmployeeId()))
                .withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getAllOrdersRequestParam(modificacionUser.getEmployeeId()))
                .withRel("allOrders"));
        user.add(links);

        LOGGER.debug("Se ha salido del método 'updateUser' del 'userService'");
        return user;
    }

    @RequestMapping(value = "/v"
            + API_VERSION, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User deleteUser(@RequestBody User userEliminar) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado el 'borrado' de un usuario");
        User user = userService.deleteUser(userEliminar);

        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(UserController.class).getUserRequestParam(user.getEmployeeId())).withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getUserPathVariable(user.getEmployeeId())).withSelfRel());
        links.add(linkTo(methodOn(UserController.class).getAllOrdersRequestParam(user.getEmployeeId()))
                .withRel("allOrders"));
        user.add(links);

        LOGGER.debug("Se ha salido del método 'deleteUser' del 'userService'");
        return user;
    }

    @RequestMapping(value = "/orders/v"
            + API_VERSION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Orders getAllOrdersRequestParam(
            @RequestParam(value = "employee", required = true) String employeeId) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'lectura' de todas las ordenes del usuario con employeeId: " + employeeId);
        List<Order> allOrders = userService.getAllOrders(employeeId);
        Orders orders = new Orders(allOrders);

        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(UserController.class).getUserRequestParam(employeeId)).withRel("employee"));
        links.add(linkTo(methodOn(UserController.class).getUserPathVariable(employeeId)).withRel("employee"));
        links.add(linkTo(methodOn(UserController.class).getAllOrdersRequestParam(employeeId)).withSelfRel());
        orders.add(links);

        LOGGER.debug("Se ha salido del método 'getAllOrders' del 'userService'");
        return orders;
    }

}
