package com.example.rest.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.rest.bean.Order;
import com.example.rest.service.OrderService;
import com.example.rest.exception.ApiGeneralException;

@RestController
@RequestMapping("/order")
public class OrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(Order.class);
    private static final String API_VERSION = "2.0";

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/v"
            + API_VERSION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Order getOrder(@RequestParam(value = "order-id", required = true) String orderId) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'lectura' de una orden");
        Order order = orderService.readOrder(orderId);

        List<Link> links = new ArrayList<Link>();
        links.add(linkTo(methodOn(OrderController.class).getOrder(order.getOrderId())).withSelfRel());
        order.add(links);

        LOGGER.debug("Se ha salido del método 'readOrder' del service");
        return order;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/v"
            + API_VERSION, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Order createOrder(@RequestBody Order nuevoOrder) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'creación' de una orden");
        Order order = orderService.createOrder(nuevoOrder);
        LOGGER.debug("Se ha salido del método 'createOrder' del service");
        return order;
    }

    @RequestMapping(value = "/v"
            + API_VERSION, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Order updateOrder(@RequestBody Order modificacionOrder, @RequestParam(value = "employee-id", required = true)  String employeeId)
            throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado la 'actualización' de una orden");
        Order order = orderService.updateOrder(modificacionOrder, employeeId);
        LOGGER.debug("Se ha salido del método 'updateOrder' del service");
        return order;
    }

    @RequestMapping(value = "/v"
            + API_VERSION, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Order deleteOrder(@RequestBody Order orderEliminar) throws ApiGeneralException {
        LOGGER.debug("Se ha solicitado el 'borrado' de un orden");
        Order order = orderService.deleteOrder(orderEliminar);
        LOGGER.debug("Se ha salido del método 'deleteOrder' del service");
        return order;
    }

}
