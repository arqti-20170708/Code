package com.example.rest.exception;

import org.springframework.http.HttpStatus;

/**
 * {@inheritDoc}
 * 
 * Clase que extiende de {@link com.example.rest.exception.ApiGeneralException}
 * para manejar el <b><i>HTTP Status code 400 (Bad Request)</i></b> resultado de
 * una llamada a la API y posteriormente procesarlos por
 * {@link com.example.rest.exception.handler.CustomRestExceptionHandler}
 */
public class BadRequestStatusException extends ApiGeneralException {

    private static final long serialVersionUID = 8876317313693219829L;
    private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param referencia
     *            C&oacute;digo de referencia generado para revisar en el
     *            archivo de historial la traza del error
     * @param description
     *            Descripci&oacute;n del error
     * @param info
     *            Informaci&oacute;n adicional relacionada con el error
     * @param message
     *            Mensaje de error a desplegar
     */
    public BadRequestStatusException(int referencia, String description, String info, String message) {
        super(BadRequestStatusException.HTTP_STATUS, referencia, description, info, message);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BadRequestStatusException " + super.toString();
    }
}
