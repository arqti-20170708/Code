package com.example.rest.exception.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.rest.exception.ApiError;
import com.example.rest.exception.ApiGeneralException;

/**
 * {@inheritDoc}
 * 
 * Controlador que extiende a la clase abstracta
 * {@link org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler}
 * para el manejo de errores que pueden ocurrir como resultado de la llamada a
 * la API
 */
@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Manejador para las excepciones de la clase
     * {@link com.example.rest.exception.ApiGeneralException} o de clases que
     * la extienden.
     * 
     * @param age
     *            excepci&oacute;n de la clase
     *            {@link com.example.rest.exception.ApiGeneralException} o de
     *            clases que la extienden
     * @return Entidad de respuesta del tipo gen&eacute;rico
     *         {@link com.example.rest.exception.ApiError}
     * @see org.springframework.web.bind.annotation.ExceptionHandler
     * @see org.springframework.http.ResponseEntity
     */
    @ExceptionHandler(ApiGeneralException.class)
    public ResponseEntity<ApiError> apiGeneralExceptionHandler(ApiGeneralException age) {
        ApiError apiError = new ApiError(age.getHttpStatus(), age.getMessage(), age.getDescription());
        return new ResponseEntity<ApiError>(apiError, apiError.getStatus());
    }

    /**
     * Manejador para las excepciones de la clase {@link java.lang.Exception} y
     * que no han sido manejadas por
     * {@link com.example.rest.exception.handler.CustomRestExceptionHandler#apiGeneralExceptionHandler(ApiGeneralException)}
     * 
     * @param e
     *            excepci&oacute;n de la clase {@link java.lang.Exception} y que
     *            no han sido manejadas por
     *            {@link com.example.rest.exception.handler.CustomRestExceptionHandler#apiGeneralExceptionHandler(ApiGeneralException)}
     * @return Entidad de respuesta del tipo gen&eacute;rico
     *         {@link com.example.rest.exception.ApiError}
     * @see org.springframework.web.bind.annotation.ExceptionHandler
     * @see org.springframework.http.ResponseEntity
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> exceptionHandler(Exception e) {
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiError apiError = new ApiError(httpStatus, e.getMessage(), getErrorsFromStackTrace(e.getStackTrace()));
        return new ResponseEntity<ApiError>(apiError, apiError.getStatus());
    }

    // Internal helpers
    private List<String> getErrorsFromStackTrace(StackTraceElement[] stackTraceElements) {
        List<String> errors = new ArrayList<String>();
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            String error = "Fallo en '" + stackTraceElement.getMethodName() + "' de clase '"
                    + stackTraceElement.getClassName() + "' [número de línea: " + stackTraceElement.getLineNumber()
                    + "]";
            errors.add(error);
        }
        return errors;
    }
}
