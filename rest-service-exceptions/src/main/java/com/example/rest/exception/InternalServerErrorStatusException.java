package com.example.rest.exception;

import org.springframework.http.HttpStatus;

/**
 * {@inheritDoc}
 * 
 * Clase que extiende de {@link com.example.rest.exception.ApiGeneralException}
 * para manejar el <b><i>HTTP Status code 500 (Internal Server Error)</i></b>
 * resultado de una llamada a la API y posteriormente procesarlos por
 * {@link com.example.rest.exception.handler.CustomRestExceptionHandler}
 */
public class InternalServerErrorStatusException extends ApiGeneralException {

    private static final long serialVersionUID = -871568132886659471L;
    private static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param referencia
     *            C&oacute;digo de referencia generado para revisar en el
     *            archivo de historial la traza del error
     * @param description
     *            Descripci&oacute;n del error
     * @param info
     *            Informaci&oacute;n adicional relacionada con el error
     * @param message
     *            Mensaje de error a desplegar
     */
    public InternalServerErrorStatusException(int referencia, String description, String info, String message) {
        super(InternalServerErrorStatusException.HTTP_STATUS, referencia, description, info, message);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InternalServerErrorStatusException " + super.toString();
    }
}
