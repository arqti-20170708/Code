package com.example.rest.exception;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

/**
 * {@inheritDoc}
 * 
 * Clase utilizada para el env&iacute;o del <i>body</i> de la respuesta a una
 * llamada HTTP a la API cuando ocurre un error que ha sido manejado y
 * posteriormente procesado por
 * {@link com.example.rest.exception.handler.CustomRestExceptionHandler}
 */
public class ApiError implements Serializable {

    private static final long serialVersionUID = 866029508161747810L;

    private HttpStatus status;
    private String message;
    private List<String> errors;

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param status
     *            C&oacute;digo de respuesta para la petici&oacute;n HTTP
     * @param message
     *            Mensaje de error a desplegar
     * @param errors
     *            Listado de cadenas de error
     */
    public ApiError(HttpStatus status, String message, List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param status
     *            C&oacute;digo de respuesta para la petici&oacute;n HTTP.
     * @param message
     *            Mensaje de error a desplegar
     * @param error
     *            Cadena de error
     */
    public ApiError(HttpStatus status, String message, String error) {
        super();
        this.status = status;
        this.message = message;
        this.errors = Arrays.asList(error);
    }

    /**
     * @return C&oacute;digo de respuesta para la petici&oacute;n HTTP
     */
    public HttpStatus getStatus() {
        return status;
    }

    /**
     * @param status
     *            C&oacute;digo de respuesta para la petici&oacute;n HTTP
     */
    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    /**
     * @return Mensaje de error a desplegar
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            Mensaje de error a desplegar
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return Listado de cadenas de error
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * @param errors
     *            Listado de cadenas de error
     */
    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ApiError [status=" + status + ", message=" + message + ", errors=" + errors + "]";
    }
}
