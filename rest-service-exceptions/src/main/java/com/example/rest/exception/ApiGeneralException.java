package com.example.rest.exception;

import java.util.Random;

import org.springframework.http.HttpStatus;

/**
 * {@inheritDoc}
 * 
 * Clase de la que extienden aquellas que representan un <b><i>HTTP Status
 * code</i></b> que debe ser manejado y posteriormente procesado por
 * {@link com.example.rest.exception.handler.CustomRestExceptionHandler}
 */
public class ApiGeneralException extends Exception {

    private static final long serialVersionUID = 3851138912343684273L;
    private static final int MIN = 1;
    private static final int MAX = Integer.MAX_VALUE;
    private static final Random RAND = new Random();
    private HttpStatus httpStatus;
    private int referencia;
    private String description;
    private String info;

    /**
     * Sobrecarga privada de constructor, cuando no se invoca el constructor con
     * paso de par&aacute;metros se asume que ha ocurrido un error interno del
     * servidor
     */
    @SuppressWarnings("unused")
    private ApiGeneralException() {
        super();
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        this.referencia = 0;
        this.description = "";
        this.info = "";
    }

    /**
     * Sobrecarga de constructor con paso de par&aacute;metros
     * 
     * @param httpStatus
     *            C&oacute;digo de respuesta para la petici&oacute;n HTTP
     * @param referencia
     *            C&oacute;digo de referencia generado para revisar en el
     *            archivo de historial la traza del error
     * @param description
     *            Descripci&oacute;n del error
     * @param info
     *            Informaci&oacute;n adicional relacionada con el error
     * @param message:
     *            Mensaje de error a desplegar
     */
    public ApiGeneralException(HttpStatus httpStatus, int referencia, String description, String info, String message) {
        super(message);
        this.httpStatus = httpStatus;
        this.referencia = referencia;
        this.description = description;
        this.info = info;
    }

    /**
     * @return C&oacute;digo de respuesta para la petici&oacute;n HTTP
     */
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    /**
     * @param httpStatus
     *            C&oacute;digo de respuesta para la petici&oacute;n HTTP
     */
    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    /**
     * @return C&oacute;digo de referencia generado para revisar en el archivo
     *         de historial la traza del error
     */
    public int getReferencia() {
        return referencia;
    }

    /**
     * @param referencia
     *            C&oacute;digo de referencia generado para revisar en el
     *            archivo de historial la traza del error
     */
    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    /**
     * @return Descripci&oacute;n del error
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            Descripci&oacute;n del error
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Informaci&oacute;n adicional relacionada con el error
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info
     *            Informaci&oacute;n adicional relacionada con el error
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * M&eacute;todo est&aacute;tico que devuelve un entero entre 1 y
     * {@link java.lang.Integer#MAX_VALUE} el cual es usado para referencia una
     * vez que ha ocurrido un error
     * 
     * @return Entero usado como referencia una vez que ha ocurrido un error
     */
    public static int randInt() {
        return ApiGeneralException.RAND.nextInt((ApiGeneralException.MAX - ApiGeneralException.MIN) + 1)
                + ApiGeneralException.MIN;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "[httpStatus=" + httpStatus + ", referencia=" + referencia + ", description=" + description + ", info="
                + info + "]";
    }
}
